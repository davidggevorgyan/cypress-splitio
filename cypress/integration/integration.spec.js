import { skipOn, onlyOn } from '@cypress/skip-test';


describe( 'integration tests', () => {
	onlyOn( Cypress.env( 'feature-one' ) === 'on', () => {
		it( 'should run this test when "feature-one"  === "on" condition is true', () => {
			expect( true ).to.equal( true );
		} );
	} );

	skipOn( Cypress.env( 'feature-two' ) === 'off', () => {
		it( 'should skip this test when "feature-two"  === "off" condition is true', () => {
			expect( false ).to.equal( true );
		} );
	} );

	it( 'should have valid data in generated fixtures', () => {
		cy.fixture( 'splits' ).then( ( json ) => {
			expect( json['feature-one'] ).to.equal( 'on' );
			expect( json['feature-two'] ).to.equal( 'off' );
			expect( json['feature-three'] ).to.equal( 'custom' );
		} );
	} );

	it( 'should have valid splits defined as env variables', () => {
		expect( Cypress.env( 'feature-one' ) ).to.equal( 'on' );
		expect( Cypress.env( 'feature-two' ) ).to.equal( 'off' );
		expect( Cypress.env( 'feature-three' ) ).to.equal( 'custom' );
	} );

	it( 'should return valid splits based on custom configuration', () => {
		expect( Cypress.env( 'feature-four' ) ).to.equal( 'on' );
	} );

} );

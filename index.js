// / <reference types="cypress" />

const { SplitFactory } = require( '@splitsoftware/splitio' );
const fs = require( 'fs' );

const pluginError = ( mes ) => {
	throw new Error( `[cypress-splitio]: ${ mes }` );
};

const generateFixtureFile = ( dir, splitJSON, fixture ) => {
	if ( !fixture ) {
		return;
	}

	if ( !fs.existsSync( dir ) ) {
		fs.mkdirSync( dir );
	}

	fs.writeFileSync( `${ dir }splits.json`, JSON.stringify( splitJSON ) );
};

/**
 * @type {Cypress.PluginConfig}
 */
module.exports = ( config, fixture = true ) => new Promise( ( resolve, reject ) => {
	const CONFIG = config.env.SPLITIO_CONFIG;
	let KEY;
	let ATTRIBUTES;
	if ( !CONFIG ) {
		resolve( { SPLITKEY: 'undefined' } );
		return config;
	}

	if ( typeof CONFIG === 'object' ) {
		ATTRIBUTES = CONFIG;
		KEY = CONFIG.KEY;
		delete ATTRIBUTES.KEY;
	} else {
		KEY = config.env.SPLITIO_CONFIG;
	}


	let splitNames;
	let splitNamesAndValues;
	const factory = SplitFactory( {
		core: {
			authorizationKey: KEY,
			key: 'key',
		},
		startup: {
			readyTimeout: 1.5,
		},
	} );

	const manager = factory.manager();
	manager.once( manager.Event.SDK_READY, () => {
		splitNames = manager.names();
	} );
	manager.once( manager.Event.SDK_READY_TIMED_OUT, ( error ) => {
		pluginError( 'Can\'t get splits. Check your connection or API key' );
		reject( error );
	} );

	const client = factory.client();
	client.on( client.Event.SDK_READY, () => {
		splitNamesAndValues = client.getTreatments( 'key', splitNames, ATTRIBUTES );
		client.destroy();
		generateFixtureFile( './cypress/fixtures/', splitNamesAndValues, fixture );
		resolve( JSON.parse( `{ "env": ${ JSON.stringify( splitNamesAndValues ) } }` ) );
	} );
	client.on( client.Event.SDK_READY_TIMED_OUT, ( error ) => {
		client.destroy();
		pluginError( 'Can\'t get splits. Check your connection or API key' );
		reject( error );
	} );
	return config;
} );

const getSplits = require( '../index' );

// eslint-disable-next-line arrow-body-style
test( 'should resolve the promise when split API key in not provided', async () => {
	return expect( getSplits( { env: '' } ) ).resolves.toMatchObject( { SPLITKEY: 'undefined' } );
} );
